import React from "react";
import { connect } from 'react-redux';
import { TabContent, TabPane, Row } from 'reactstrap';
import ChatToggleButton from "./ChatToggleButton";
import ChatTabs from "./ChatTabs";
import ChatMessage from "./ChatMessage";
import "./style.css";

function Chat(props) {
    return (
        <div className={"chat " + (props.showChat ? 'chat_open' : 'chat_close')}>
            <ChatToggleButton showChat={props.showChat} toggleChat={props.toggleChat} />
            <ChatTabs tabs={props.tabs} activeTab={props.activeTab} changeTab={props.changeTab} />
            <TabContent activeTab={props.activeTab} className="chat__content">
                {props.tabs.map((tab) => {
                    return (
                        <TabPane tabId={tab.id} key={tab.id}>
                            <Row>
                                {tab.messages.map((message) => {
                                    return (<ChatMessage message={message} currentUser={props.user} key={message.id} />)
                                })}
                            </Row>
                        </TabPane>
                    )
                })}
            </TabContent>
        </div>
    );
}
export default connect(state => ({
    showChat: state.chat.showChat,
    activeTab: state.chat.activeTab,
    tabs: state.chat.tabs
}),
    dispatch => ({
        toggleChat: () => {
            dispatch({ type: 'CHAT_TOGGLE' });
        },
        changeTab: (id) => {
            dispatch({ type: 'CHAT_CHANGE_TAB', payload: id });
        },
    }))(Chat);