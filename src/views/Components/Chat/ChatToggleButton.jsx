import React from "react";
import { Button, Fade } from 'reactstrap';

export default function ChatToggleButton({ showChat, toggleChat}) {
  return (
    <Button color={showChat ? "danger" : 'success'} className="chat__toggle" onClick={toggleChat}>
      <Fade in={showChat}>
        {showChat ? <i className="icon-close icons font-3xl text-white"></i> : ''}
      </Fade>
      <Fade in={!showChat}>
        {!showChat ? <i className="icon-speech icons font-3xl text-white"></i> : ''}
      </Fade>
    </Button>
  )
}