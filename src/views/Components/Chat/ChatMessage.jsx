import React from "react";
import { Col } from 'reactstrap';

export default function ChatMessage({ message, currentUser }) {
  if (currentUser.id === message.user.id) {
    return (
      <Col sm="12" className="d-flex mb-1 justify-content-end">
        <p className="chat__user-message">{message.content.text}</p>
      </Col>
      )
  } else {
    return (
      <Col sm="12" className="d-flex mb-1">
        <div className="chat__avatar">
          <img className="img-avatar img-fluid" src={message.user.avatar} alt="#" />
        </div>
        <div className="chat__text">
          <p className="chat__title">{message.user.name}</p>
          <p className="chat__message">{message.content.text}</p>
        </div>
      </Col>
    )
  }
}