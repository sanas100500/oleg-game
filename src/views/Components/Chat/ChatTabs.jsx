import React from "react";
import { Nav, NavItem, NavLink } from 'reactstrap';

export default function ChatTabs({ tabs, activeTab, changeTab }) {
  return (
    <Nav tabs className="chat__tabs">
      {tabs.map((el, id) => {
        return (
          <NavItem key={el.id}>
            <NavLink
              className={(activeTab === '1' ? 'active chat__link_active' : '') + ' chat__link'}
              onClick={() => { changeTab('1'); }}>
              {el.name}
            </NavLink>
          </NavItem>
        )
      })}
    </Nav>
  )
}