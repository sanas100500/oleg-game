import React, { Component } from "react";
import { Button, Col } from "reactstrap";

class PlayerAttack extends Component {
    colors = ['primary', 'success', 'danger']
    actionsDefault = [0, 0, 0, 0, 0]
    kmb = [{
        name: 'rock',
        actions: [0, 2, 1, 1, 2]
    }, {
        name: 'paper',
        actions: [1, 0, 2, 2, 1]
    }, {
        name: 'scissors',
        actions: [2, 1, 0, 1, 2]
    }, {
        name: 'lizard',
        actions: [2, 1, 2, 0, 1]
    }, {
        name: 'spock',
        actions: [1, 2, 1, 2, 0]
    }]
    constructor() {
        super();
        this.hover = this.hover.bind(this);
        this.hoverLeave = this.hoverLeave.bind(this);
        this.state = {
            actions: this.actionsDefault
        }
    }
    hover(id) {
        this.setState({ actions: this.kmb[id].actions });
    }
    setColor(id) {
        return this.colors[this.state.actions[id]]
    }
    hoverLeave() {
        this.setState({ actions: this.actionsDefault });
    }
    render() {
        return (
            <Col xs={4} className="px-2 d-flex flex-column justify-content-between">
                {
                    this.kmb.map((el, i) => {
                        return (
                            <Button key={el.name} outline color={this.setColor(i)} size={'lg'} block className="text-uppercase"
                                onMouseEnter={(e) => this.hover(i)} onMouseLeave={this.hoverLeave}>
                                {el.name}
                            </Button>
                        )
                    })
                }
            </Col>
        )
    }
}

export default PlayerAttack;