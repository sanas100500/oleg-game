import React, { Component, Fragment } from "react";
import { Fade, Button } from "reactstrap";
import IconSwords, { IconCancel } from '../icons/';
import MyTooltip from '../MyTooltip/';

class TakeFight extends Component {
    attackTooltip = 'attackTooltip-'
    attackText = 'Атаковать'
    cancelText = 'Отменить атаку'
    
    constructor(props) {
        super(props);
        this.state = {
            fadeIn: true,
            tooltipText: this.attackText
        };
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            fadeIn: !this.state.fadeIn,
            tooltipText: this.state.fadeIn ? this.cancelText : this.attackText
        });
    }

    render() {
        return (
            <Fragment>
                <Button color={this.state.fadeIn ? 'success' :'danger'} className='p-1' id={this.attackTooltip + this.props.tooltipID} onClick={this.toggle}>
                    <Fade in={this.state.fadeIn} className="d-flex align-items-center">
                        {this.state.fadeIn ? <IconSwords /> : ''}
                    </Fade>
                    <Fade in={!this.state.fadeIn} className="d-flex align-items-center">
                        {this.state.fadeIn ? '' : <IconCancel />}
                    </Fade>
                </Button>
                <MyTooltip placement="top" target={this.attackTooltip + this.props.tooltipID} text={this.state.tooltipText} />
            </Fragment>
        )
    }
}

export default TakeFight;