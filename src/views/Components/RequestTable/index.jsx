import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import PlayerAva from '../PlayerAva/';
import TakeFight from '../TakeFight/';


class RequestTable extends Component {
    constructor(props) {
        super(props);
        this.getArray = this.getArray.bind(this)
        this.state = {
            users: this.getArray()
        };
    }
    getArray() {
        let array = [];
        for (let i = 0; i < 4; i++) {
            array.push(this.props.user);
        }
        return array;
    }
    render() {
        return (
            <Row className='no-gutters mb-2'>
                {
                    this.state.users.map((user, id) => {
                        return (
                            <Col lg={2} md={3} sm={4} xs={6} key={id}>
                                <Row className="img-thumbnail no-gutters justify-content-between align-items-center">
                                    <Col xs={9} className="text-capitalize font-weight-bold" style={{overflow: 'hidden'}}>
                                        <span>{this.props.user.name.split(' ')[0]}</span> <br />
                                        <span>{this.props.user.name.split(' ')[1]}</span>
                                    </Col>
                                    <Col xs={3}>
                                        <TakeFight tooltipID={id}></TakeFight>
                                    </Col>
                                </Row>
                                <Row className="no-gutters">
                                    <Col>
                                        <PlayerAva user={user} />
                                    </Col>
                                </Row>
                            </Col>
                        )
                    })
                }
            </Row>
        )
    }
}

export default RequestTable;