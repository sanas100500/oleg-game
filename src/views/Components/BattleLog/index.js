import React from "react";
import {Badge, ListGroup, ListGroupItem} from "reactstrap";

function BattleLog() {
    return (
        <ListGroup className="rounds">
            <ListGroupItem action
                           className="d-flex justify-content-between align-items-center px-1 py-0 bg-gray-200">
                Round
                <Badge color={'primary'}>3</Badge>
            </ListGroupItem>
            <ListGroupItem action
                           className="d-flex justify-content-between align-items-center py-1 px-1">
                PL.1
                <div className="d-flex flex-column">
                    <Badge color={'primary'} pill className="mb-1">attak</Badge>
                    <Badge color={'danger'} pill>dmg</Badge>
                </div>
                <div className="d-flex flex-column">
                    <Badge color={'primary'} className="mb-1">2</Badge>
                    <Badge color={'danger'}>1</Badge>
                </div>
            </ListGroupItem>
            <ListGroupItem action
                           className="d-flex justify-content-between align-items-center py-1 px-1">
                PL.2
                <div className="d-flex flex-column">
                    <Badge color={'primary'} pill className="mb-1">attak</Badge>
                    <Badge color={'danger'} pill>dmg</Badge>
                </div>
                <div className="d-flex flex-column">
                    <Badge color={'primary'} className="mb-1">2</Badge>
                    <Badge color={'danger'}>1</Badge>
                </div>
            </ListGroupItem>
            <ListGroupItem action
                           className="d-flex justify-content-between align-items-center px-1 py-0 bg-gray-200">
                Round
                <Badge color={'primary'}>2</Badge>
            </ListGroupItem>
            <ListGroupItem action
                           className="d-flex justify-content-between align-items-center py-1 px-1">
                PL.1
                <div className="d-flex flex-column">
                    <Badge color={'primary'} pill className="mb-1">attak</Badge>
                    <Badge color={'danger'} pill>dmg</Badge>
                </div>
                <div className="d-flex flex-column">
                    <Badge color={'primary'} className="mb-1">2</Badge>
                    <Badge color={'danger'}>1</Badge>
                </div>
            </ListGroupItem>
            <ListGroupItem action
                           className="d-flex justify-content-between align-items-center py-1 px-1">
                PL.2
                <div className="d-flex flex-column">
                    <Badge color={'primary'} pill className="mb-1">attak</Badge>
                    <Badge color={'danger'} pill>dmg</Badge>
                </div>
                <div className="d-flex flex-column">
                    <Badge color={'primary'} className="mb-1">2</Badge>
                    <Badge color={'danger'}>1</Badge>
                </div>
            </ListGroupItem>
            <ListGroupItem action
                           className="d-flex justify-content-between align-items-center px-1 py-0 bg-gray-200">
                Round
                <Badge color={'primary'}>1</Badge>
            </ListGroupItem>
            <ListGroupItem action
                           className="d-flex justify-content-between align-items-center py-1 px-1">
                PL.1
                <div className="d-flex flex-column">
                    <Badge color={'primary'} pill className="mb-1">attak</Badge>
                    <Badge color={'danger'} pill>dmg</Badge>
                </div>
                <div className="d-flex flex-column">
                    <Badge color={'primary'} className="mb-1">2</Badge>
                    <Badge color={'danger'}>1</Badge>
                </div>
            </ListGroupItem>
            <ListGroupItem action
                           className="d-flex justify-content-between align-items-center py-1 px-1">
                PL.2
                <div className="d-flex flex-column">
                    <Badge color={'primary'} pill className="mb-1">attak</Badge>
                    <Badge color={'danger'} pill>dmg</Badge>
                </div>
                <div className="d-flex flex-column">
                    <Badge color={'primary'} className="mb-1">2</Badge>
                    <Badge color={'danger'}>1</Badge>
                </div>
            </ListGroupItem>
        </ListGroup>
    )
}

export default BattleLog;