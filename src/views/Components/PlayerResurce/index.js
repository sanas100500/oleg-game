import React from "react";
import {Row, Col, Progress} from "reactstrap";

function PlayerParam({user}) {
    let ultimate;
    if (user.ult) {
        ultimate = <div className="player-ult">
                <Progress value={user.ult}/>
                <div className="player-progress-text">
                    <span className="text-center">{user.ult}% ULT</span>
                </div>
            </div>
    }
    return (
        <Row className="progress-row">
            <Col className="p-0">
                <div className="player-hp">
                    <Progress value={user.hp / (user.hpmax / 100)} className='mb-1'/>
                    <div className="player-progress-text">
                        <span className="text-center">{user.hp}/{user.hpmax}</span>
                    </div>
                </div>
                {ultimate}
            </Col>
        </Row>
    )
}

export default PlayerParam;