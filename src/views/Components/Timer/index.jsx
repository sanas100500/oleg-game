import React, { Component } from "react";

class Timer extends Component {
    timer
    constructor(props) {
        super(props);
        this.state = {
            number: this.props.number
        }
    }
    componentDidMount() {
        this.timer = setInterval(() => {
            if (this.state.number <= 1) {
                this.props.onEnd(this.props.user)
                return
            }
            this.setState((prevState) => {
                return { number: --prevState.number }
            })
        }, 1000)
    }
    componentWillUnmount() {
        clearInterval(this.timer)
    }
    textColor() {
        if (this.state.number < 5) {
            return 'text-danger'
        } else if (this.state.number < 10) {
            return 'text-warning'
        } else {
            return 'text-info'
        }
    }
    render() {
        return(
            <span className={`font-weight-bold font-2xl ${this.textColor()}`}>{this.state.number}</span>
        )
    }
}
export default Timer;