import React from "react";
import {Badge} from "reactstrap";
import './style.css';

function PlayerAva({user}) {
    return (
        <div style={{position: 'relative'}}>
            <Badge color={'info'} className="position-absolute m-2 px-2 font-sm text-white">LVL {user.lvl}</Badge>
            <img className="img-thumbnail" src={user.avatar} alt="#"/>
            <img className="player-ava img-thumbnail" src={user.guild.emblem} alt="#"/>
        </div>
    )
}

export default PlayerAva;