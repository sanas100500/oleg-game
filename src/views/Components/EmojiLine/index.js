import React from "react";
import {Dropdown, Col, DropdownMenu, DropdownItem, DropdownToggle} from "reactstrap";

function EmojiLine({isOpen, toggle}) {
    return (
        <Col
            className="bg-gray-200 py-2 d-flex justify-content-between align-items-center">
            <span className="h5 d-block d-lg-none">Round 1</span>
            <img className="emoji" src="./img/emoji.jpg" alt="#"/>
            <Dropdown isOpen={isOpen} toggle={toggle}
                      direction="left">
                <DropdownToggle caret className="text-left">
                    Emoji <br/> menu
                </DropdownToggle>
                <DropdownMenu>
                    <DropdownItem header>Header</DropdownItem>
                    <DropdownItem disabled>Action</DropdownItem>
                    <DropdownItem>Another Action</DropdownItem>
                    <DropdownItem divider/>
                    <DropdownItem>Another Action</DropdownItem>
                </DropdownMenu>
            </Dropdown>
        </Col>
    )
}

export default EmojiLine;