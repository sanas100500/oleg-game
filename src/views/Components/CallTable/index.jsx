import React, { Component } from "react";
import { connect } from 'react-redux';
import { Row, Col, Button, ListGroup, ListGroupItem, Badge, Fade } from "reactstrap";
import IconSwords, { IconCancel } from '../icons/';
import Timer from "../Timer/";

let id = 0;
class CallTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isCall: true
        }
        this.toogle = this.toogle.bind(this)
        this.delUser = this.delUser.bind(this)
        this.rejectAllCall = this.rejectAllCall.bind(this)
        setInterval(() => {
            id++
            this.props.onAddCallUser({
                id: id,
                name: 'Cowboy Alyosha',
                avatar: './img/player1.jpg',
                guild: {
                    emblem: './img/player1.jpg'
                },
                lvl: 1,
                hp: 10,
                hpmax: 10,

            });
        }, 5000)

    }
    toogle() {
        this.setState({
            isCall: !this.state.isCall
        })
    }
    delUser(user) {
        this.props.onDelCallUser(user)
    }
    rejectAllCall() {
        this.props.onDelAllCallUser()
    }
    render() {
        return (
            <Row className='mb-2'>
                <Col className='d-flex justify-content-center'>
                    <Button color={'success'} size={'sm'} onClick={this.toogle}>
                        <Fade in={!this.state.isCall} tag="span">{!this.state.isCall ? 'Подать Заявку' : ''}</Fade>
                        <Fade in={this.state.isCall} tag="span" onClick={this.rejectAllCall}>{this.state.isCall ? 'Отменить Заявку' : ''}</Fade>
                    </Button>
                    <Button color={'danger'} size={'sm'} >Погонять Нимоф</Button>
                </Col>
                {
                    (() => {
                        if (this.state.isCall) {
                            return (
                                <Col xs={12} className="mt-2">
                                    <ListGroup>
                                        <ListGroupItem className="d-flex justify-content-between align-items-center font-weight-bold">Игроки бросившие вам вызов
                                            <Badge pill color="info" className="text-white">{this.props.callUsers.length}</Badge>
                                        </ListGroupItem>
                                        <div style={{ height: 62 * 4 + 'px', overflowY: 'auto' }}>
                                        {this.props.callUsers.map((user) => {
                                            return (
                                                <ListGroupItem key={user.id} tag="div" className="">
                                                    <Fade tag="div" className="d-flex justify-content-between align-items-center display-5">
                                                        <div>
                                                            {user.name}
                                                            <Badge color="info" className="text-white ml-1">LVL{user.lvl}</Badge>
                                                        </div>
                                                        <Timer number={20} onEnd={this.delUser} user={user} />
                                                        <div>
                                                            <Button color='success' className='p-1 mr-1' style={{ height: '30px' }} onClick={() => console.log(1)}>
                                                                <IconSwords size="20" />
                                                            </Button>
                                                            <Button color='danger' className='p-1' style={{ height: '30px' }} onClick={() => this.delUser(user)}>
                                                                <IconCancel size="20" />
                                                            </Button>
                                                        </div>
                                                    </Fade>
                                                </ListGroupItem>
                                            )
                                            })}
                                            </div>
                                    </ListGroup>
                                </Col>
                            )
                        }
                    })()
                }
            </Row>
        )
    }
}

export default connect(
    state => ({
        callUsers: state.callUsers,
    }),
    dispatch => ({
        onAddCallUser: (user) => {
            dispatch({ type: 'ADD_CallUser', payload: user });
        },
        onDelCallUser: (user) => {
            dispatch({ type: 'DEL_CallUser', payload: user });
        },
        onDelAllCallUser: (user) => {
            dispatch({ type: 'DEL_AllCallUser' });
        }
    })
)(CallTable);