import React from "react";
import {Badge, Col, ListGroup, ListGroupItem} from "reactstrap";

function PlayerParam({user}) {
    let paramsView = () => {
        let stats = [];
        for (let stat in user.stats) {
            stats.push(
                <ListGroupItem key={stat} action className="d-flex justify-content-between align-items-center py-0 px-2 bg-gray-300 border-0">
                    {stat}
                    <Badge pill color={'primary'}>{user.stats[stat]}</Badge>
                </ListGroupItem>
            )
        }
        return stats;
    };
    return (
        <Col xs={5} className="p-0">
            <ListGroup
                className="d-flex flex-column justify-content-between h-100 py-0 py-sm-2 text-capitalize">
                <ListGroupItem action
                               className="d-flex justify-content-between align-items-center py-0 px-2 bg-gray-300 border-0 font-weight-bold">
                    stats:
                </ListGroupItem>
                {paramsView()}
            </ListGroup>
        </Col>
    )
}

export default PlayerParam;