import React from "react";
import {Row, Button} from "reactstrap";

function Skills({user}) {
    let color, icon;
    let renderSkills = user.skills.map((skill, id) => {
        switch (skill.effect) {
            case 'heal':
                color = 'success';
                icon = 'fa-leaf';
                break;
            case 'buff':
                color = 'primary';
                icon = 'fa-gavel';
                break;
            case 'dmg':
                color = 'danger';
                icon = 'fa-rocket';
                break;
            default :
                return (
                    <div key={id} className="bg-gray-300 px-3">
                    </div>
                )
        }
        return (
            <Button key={id} color={color} className="player-skill py-2 px-2">
                <i className={`fa ${icon} fa-lg`}></i>
                <span className="font-weight-bold">{skill.effect}</span>
                <span>{skill.cost}%</span>
            </Button>
        )
    });
    return (
        <Row className="justify-content-between px-3 bg-gray-200 mb-1 py-1">
            {renderSkills}
        </Row>
    )
}

export default Skills;