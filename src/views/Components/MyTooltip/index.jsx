import React, { Component } from "react";
import { Tooltip } from "reactstrap";

class MyTooltip extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this)
        this.state = {
            tooltipOpen: false,
        };
    }

    toggle() {
        this.setState({
            tooltipOpen: !this.state.tooltipOpen
        });
    }

    render() {
        return (
            <Tooltip placement={this.props.placement} isOpen={this.state.tooltipOpen} target={this.props.target} toggle={this.toggle}>
                {this.props.text}
            </Tooltip>
        )
    }
}

export default MyTooltip;