import React, { Component } from "react";
// import { Row, Col, Button } from "reactstrap";
import './style.css';
import RequestTable from '../../Components/RequestTable/'
import CallTable from '../../Components/CallTable/'
import Chat from '../../Components/Chat/'

class Main extends Component {

	render() {
		return (
			<div className="animated fadeIn">
				<Chat {...this.props}></Chat>
				<h4 className='text-center text-info'>Вы можете сразиться с другими людьми или клоном</h4>
				<CallTable {...this.props}></CallTable>
				<h4 className='text-center text-info'>Игроки подавшие заявку</h4>
				<RequestTable {...this.props}></RequestTable>
			</div>
		)
	}
}

export default Main;