import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import './style.css';
import Skills from '../../Components/Skills/'
import PlayerAva from '../../Components/PlayerAva/'
import PlayerParam from '../../Components/PlayerParam/'
import PlayerResurce from '../../Components/PlayerResurce/'
import PlayerAttack from '../../Components/PlayerAttack/'
import BattleLog from '../../Components/BattleLog/'
import EmojiLine from '../../Components/EmojiLine/'

class Main extends Component {
	constructor(props) {
		super(props);

		this.toggle = this.toggle.bind(this);
		this.state = {
			dropdownOpen: false
		};
	}

	toggle() {
		this.setState({
			dropdownOpen: !this.state.dropdownOpen
		});
	}

	render() {
		return (
			<div className="animated fadeIn">
				<Row>
					<Col>
						<Row className="my-3 d-none d-lg-flex justify-content-center">
							<span className="h1 bg-gray-300 text-center px-3 py-2"> Round 1 </span>
						</Row>
						<Row className="justify-content-around">
							<Col lg="6" className="player pr-lg-4 py-2">
								<span className="player-name h2"> {this.props.user.name} </span>
								<Row>
									<Col xs={8}>
										<Skills user={this.props.user} />
										<Row className="mb-1 bg-gray-300">
											<Col xs={7} className="p-0">
												<PlayerAva user={this.props.user} />
											</Col>
											<PlayerParam user={this.props.user} />
										</Row>
										<PlayerResurce user={this.props.user} />
									</Col>
									<PlayerAttack />
								</Row>
							</Col>
							<Col lg={6} className="player pl-lg-4 py-2 d-flex flex-column justify-content-between">
								<span className="player-name player-name_enemy h2"> {this.props.enemy.name} </span>
								<Row>
									<Col xs={8} lg={{ order: 2 }}>
										<Row className="mb-1 bg-gray-300">
											<Col xs={7} className="p-0">
												<PlayerAva user={this.props.enemy} />
											</Col>
											<Col xs={5} className="p-0 d-flex align-items-center bg-dark">
												<p className="text-center text-warning font-lg font-weight-bold p-1 m-0"> The enemy chose on attack ! </p>
											</Col>
										</Row>
										<PlayerResurce user={this.props.enemy} />
									</Col>
									<Col xs={{ size: 4, order: 1 }} className="px-2">
										<BattleLog />
									</Col>
								</Row>
								<Row className="order-first order-lg-1 pl-lg-2 pr-2 pr-lg-0 pb-3 pb-lg-0">
									<EmojiLine isOpen={this.state.dropdownOpen} toggle={this.toggle} />
								</Row>
							</Col>
						</Row>
					</Col>
				</Row>
			</div>
		)
	}
}

export default Main;