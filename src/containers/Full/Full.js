import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Container } from 'reactstrap';
import Header from '../../components/Header/';
import Sidebar from '../../components/Sidebar/';
import Breadcrumb from '../../components/Breadcrumb/';
import Footer from '../../components/Footer/';
import Battle from '../../views/Pages/Battle';
import Main from '../../views/Pages/Main';

class Full extends Component {
  render() {
    return (
      <div className="app">
        <Header {...this.props}/>
        <div className="app-body">
          <Sidebar {...this.props} />
          <main className="main">
            <Breadcrumb />
            <Container>
              <Switch>
                <Route path="/main" name="Main" render={(props) => (
                  <Main {...props} {...this.props}/>
                )} />
                <Route path="/battle" name="Battle" render={(props) => (
                  <Battle {...props} {...this.props} />
                )} />
                <Redirect from="/" to="/main" />
              </Switch>
            </Container>
          </main>
        </div>
        <Footer />
      </div>
    );
  }
}

export default connect(state => (
  {
    user: state.user,
    enemy: state.enemy
  }
), dispatch => (
  {

  }))(Full);
