import { combineReducers } from 'redux';
import user from './user';
import enemy from './enemy';
import battle from './battle';
import callUsers from './callUsers';
import chat from './chat';

export default combineReducers({
    user,
    enemy,
    battle,
    callUsers,
    chat
})