let initialState = {
    showChat: true,
    activeTab: '1',
    tabs: [{
        id: '1',
        name: 'Общий',
        messages: [{
            id: 1,
            user: {
                id: 4,
                avatar: './img/player1.jpg',
                name: 'Huy Papin'
            },
            content: {
                text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis eligendi similique consequatur perferendis doloribus sunt aut ut, voluptates eius quibusdam.'
            }
        }, {
            id: 2,
            user: {
                id: 3,
                avatar: './img/player1.jpg',
                name: 'Huy Mamin'
            },
            content: {
                text: 'Lorem ipsum dolor sit amet'
            }
        }, {
            id: 3,
            user: {
                id: 1,
                avatar: './img/player1.jpg',
                name: 'Cowboy Alyosha'
            },
            content: {
                text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'
            }
        }]
    }]
};
export default function user(state = initialState, action) {
    switch (action.type) {
        case 'CHAT_TOGGLE':
            return toggle(state)
        case 'CHAT_CHANGE_TAB':
            return changeTab(state, action.payload)
        default:
            return state;
    }
}
function toggle(state) {
    return {
        ...state,
        showChat: !state.showChat
    }
}
function changeTab(state, id) {
    return {
        ...state,
        activeTab: id
    }
}
