let initialState = {
    id: 2,
    name: 'Mamin Huj',
    avatar: './img/player2.jpg',
    guild: {
      emblem: './img/player2.jpg'
    },
    lvl: 2,
    hp: 11,
    hpmax: 11,

};
export default function enemy(state = initialState, action) {
    if (action.type === 'ADD_TRACK') {
        return [
            ...state,
            action.payload
        ];
    }
    return state;
}