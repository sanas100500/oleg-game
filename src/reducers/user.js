let initialState = {
    id: 1,
    name: 'Cowboy Alyosha',
    avatar: './img/player1.jpg',
    guild: {
      emblem: './img/player1.jpg'
    },
    lvl: 1,
    hp: 10,
    hpmax: 10,
    ult: 25,
    stats: {
        def: 1,
        dmg: 3,
        matk: 30,
        mdef: 30,
    },
    skills: [{
        id: 1,
        effect: 'heal',
        cost: 25
    },{
        id: 2,
        effect: 'buff',
        cost: 30
    },{
        id: 3,
        effect: 'dmg',
        cost: 35
    },{
        id: 4,
        effect: 'empty'
    }]

};
export default function user(state = initialState, action) {
    if (action.type === 'ADD_TRACK') {
        return [
            ...state,
            action.payload
        ];
    }
    return state;
}