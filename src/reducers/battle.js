let initialState = {
    log: [{
        user: {
            atk: 2,
            dmg: 1
        },
        enemy: {
            atk: 3,
            dmg: 3
        }
    },{
        user: {
            atk: 2,
            dmg: 1
        },
        enemy: {
            atk: 3,
            dmg: 3
        }
    },{
        user: {
            atk: 2,
            dmg: 1
        },
        enemy: {
            atk: 3,
            dmg: 3
        }
    }]
};
export default function enemy(state = initialState, action) {
    if (action.type === 'ADD_TRACK') {
        return [
            ...state,
            action.payload
        ];
    }
    return state;
}