let initialState = [];
export default function user(state = initialState, action) {
    switch (action.type) {
        case 'DEL_CallUser':
            return del(action.payload, state)
        case 'ADD_CallUser':
            return [
                ...state,
                action.payload,
            ]
        case 'DEL_AllCallUser':
            return []
        default:
            return state;
    }
}

function del(user, state) {
    let delUserID = state.indexOf(user)
    return [
        ...state.slice(0, delUserID),
        ...state.slice(delUserID + 1)
    ]
}