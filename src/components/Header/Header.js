import React, {Component} from 'react';
import {
    Badge,
    Dropdown,
    DropdownMenu,
    DropdownItem,
    Nav,
    NavItem,
    // NavLink,
    NavbarToggler,
    NavbarBrand,
    DropdownToggle
} from 'reactstrap';
import {Link} from 'react-router-dom';

class Header extends Component {

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            dropdownOpen: false
        };
    }

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    sidebarToggle(e) {
        e.preventDefault();
        document.body.classList.toggle('sidebar-hidden');
    }

    sidebarMinimize(e) {
        e.preventDefault();
        document.body.classList.toggle('sidebar-minimized');
    }

    mobileSidebarToggle(e) {
        e.preventDefault();
        document.body.classList.toggle('sidebar-mobile-show');
    }

    asideToggle(e) {
        e.preventDefault();
        document.body.classList.toggle('aside-menu-hidden');
    }

    render() {
        return (
            <header className="app-header navbar justify-content-between">
                <NavbarToggler className="d-lg-none" onClick={this.mobileSidebarToggle}>&#9776;</NavbarToggler>
                <NavbarToggler className="d-md-down-none" onClick={this.sidebarToggle}>&#9776;</NavbarToggler>
                <NavbarBrand tag={Link} to={'/'}></NavbarBrand>
                <Nav className="mr-1" navbar>
                    <NavItem>
                        <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                            <DropdownToggle className="nav-link dropdown-toggle">
                                <span className="d-md-down-none">{this.props.user.name}</span>
                                <img src={this.props.user.avatar} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                            </DropdownToggle>
                            <DropdownMenu right className={this.state.dropdownOpen ? 'show' : ''} style={{minWidth: '180px'}}>
                                <DropdownItem header tag="div"
                                              className="text-center"><strong>Account</strong></DropdownItem>
                                <DropdownItem><i className="fa fa-bell-o"></i> Updates<Badge
                                    color="info">42</Badge></DropdownItem>
                                <DropdownItem><i className="fa fa-envelope-o"></i> Messages<Badge
                                    color="success">42</Badge></DropdownItem>
                                <DropdownItem><i className="fa fa-tasks"></i> Tasks<Badge
                                    color="danger">42</Badge></DropdownItem>
                                <DropdownItem><i className="fa fa-comments"></i> Comments<Badge
                                    color="warning">42</Badge></DropdownItem>
                                <DropdownItem header tag="div"
                                              className="text-center"><strong>Settings</strong></DropdownItem>
                                <DropdownItem><i className="fa fa-user"></i> Profile</DropdownItem>
                                <DropdownItem><i className="fa fa-wrench"></i> Settings</DropdownItem>
                                <DropdownItem><i className="fa fa-usd"></i> Payments<Badge color="secondary">42</Badge></DropdownItem>
                                <DropdownItem><i className="fa fa-file"></i> Projects<Badge
                                    color="primary">42</Badge></DropdownItem>
                                <DropdownItem divider/>
                                <DropdownItem><i className="fa fa-shield"></i> Lock Account</DropdownItem>
                                <DropdownItem><i className="fa fa-lock"></i> Logout</DropdownItem>
                            </DropdownMenu>
                        </Dropdown>
                    </NavItem>
                </Nav>
            </header>
        )
    }
}

export default Header;
